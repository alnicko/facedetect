#!/usr/bin/env bash

AMP_POWERDOWN_GPIO=1

. ./gpio_common.sh


amp_init()
{
    gpio_export ${AMP_POWERDOWN_GPIO}
    gpio_setup ${AMP_POWERDOWN_GPIO} out 0
}

amp_release()
{
    amp_disable
    gpio_unexport ${AMP_POWERDOWN_GPIO}
}

amp_enable()
{
    gpio_set_value ${AMP_POWERDOWN_GPIO} 1
}

amp_disable()
{
    gpio_set_value ${AMP_POWERDOWN_GPIO} 0
}
