#!/usr/bin/env bash

W=${1}
H=${2}
PHOTO_NAME=${3}

FPS=30
if [[ ${W} -ge 1920 ]]; then
    FPS=15
fi

gst-launch-1.0 imxv4l2src device=/dev/video0 num-buffers=1 \
    ! video/x-raw,format=I420,width=${W},height=${H},framerate=${FPS}/1 \
    ! vpuenc_jpeg ! jpegparse ! filesink location=${PHOTO_NAME}.jpeg
