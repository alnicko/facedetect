# Helper scripts:

* WiFi:

    * Connect to WiFi

        ```bash
        wifi_config SSID PASS
        ```
    
    * Show current WiFi credentials:
        
        ```bash
        wifi_config -g
        ```
    
    * Remove WiFi credentials and disconnect from AP:
    
        ```bash
        wifi_config -r
        ```

* Camera

    * Grab photo with resolution 640x480
    
        ```bash
        ./get_photo.sh 640 480 photo_640x480
        ```
    
    * Record 10 seconds video with resolution 1920x1080 and bitrate 10M bit
    
        ```bash
        ./record_video.sh 1920 1080 10000 vid_1920x1080_10Mb 10
        ```

    * Start video stream

        ```bash
        gst-launch-1.0 imxv4l2src device=/dev/video0 ! \
            video/x-raw,format=I420,width=1920,height=1080,framerate=15/1 ! \
            queue ! vpuenc_h264 bitrate=5000 ! \
            h264parse ! flvmux streamable=true ! queue ! \
            rtmpsink location=<STREAM_SERVER_URL>
        ```

    * Start/Stop facedetect demo application

        ```bash
        # To stop
        systemctl stop facedetect-demo
        # To start
        systemctl start facedetect-demo
        ```


* LED

    LEDs are controlled by PWM channels. Board has 3 channels: 0, 1, 3.
    
    * Source PWM commands
    
        ```bash
        . ./pwm_test.sh
        ```
    
    * Setup channel 0, period 1 ms, duty 0:
    
        ```bash
        pwm_setup 0 1000000 0
        ```
    
    * Set duty to 0.5 ms on channel 0
    
        ```bash
        pwm_set_duty 0 500000
        ```
    
    * Disable channel 0
        
        ```bash
        pwm_disable 0
        ```
    
    
    
    