#!/usr/bin/env bash

WPA_CONF_PATH=/etc/wpa_supplicant.conf
CONNMAN_CONFIG_FILE=/var/lib/connman/ap_work.config

print_usage()
{
    echo "-h/--help         Show help options"
    echo "-s/--set          Setup WiFi AP credentials:"
    echo "                      $0 --set <SSID> <PASSWORD>"
    echo "-r/--remove       remove WiFi AP credentials"
    echo "-g/--get          Show current WiFi AP credentials"
    exit 1
}

restore_wpa_supplicant()
{
    echo "ctrl_interface=/var/run/wpa_supplicant" > ${WPA_CONF_PATH}
    echo "ctrl_interface_group=0" >> ${WPA_CONF_PATH}
    echo "update_config=1" >> ${WPA_CONF_PATH}
    echo "" >> ${WPA_CONF_PATH}
    echo "network={" >> ${WPA_CONF_PATH}
    echo "        key_mgmt=NONE" >> ${WPA_CONF_PATH}
    echo "}" >> ${WPA_CONF_PATH}
}

wifi_setup()
{
    local SSID=$1
    local PASSWORD=$2
    local MODE=$3

    echo "SSID: '${SSID}', PASSWORD: '${PASSWORD}'"

    if [[ "${SSID}" == "" ]] || [[ "${PASSWORD}" == "" ]]; then
        print_usage
    fi

    wpa_passphrase "${SSID}" "${PASSWORD}" > /dev/null
    if [[ $? -ne 0 ]]; then
        wpa_passphrase "${SSID}" "${PASSWORD}"
        exit 1
    fi

    if [[ "${MODE}" == "wpa" ]]; then
        wpa_passphrase "${SSID}" "${PASSWORD}" >> ${WPA_CONF_PATH}
    else
        restore_wpa_supplicant

        cat << EOF > ${CONNMAN_CONFIG_FILE}
[service_working_wifi]
Type = wifi
Name = ${SSID}
Passphrase = ${PASSWORD}
IPv4 = dhcp
EOF
    fi
}

wifi_remove()
{
    if [[ "${1}" == "wpa" ]]; then
        restore_wpa_supplicant
    else
        rm -f ${CONNMAN_CONFIG_FILE}
    fi
}

show_current()
{
    ssid=""
    pass=""

    if [[ "${1}" == "wpa" ]]; then
        ssid=`cat ${WPA_CONF_PATH} | grep -o -P '(?<=ssid=").*(?=")'`
        pass=`cat ${WPA_CONF_PATH} | grep -o -P '(?<=#psk=").*(?=")'`
    else
        if [[ -f ${CONNMAN_CONFIG_FILE} ]]; then
            ssid=`grep Name ${CONNMAN_CONFIG_FILE} | awk {'print $3'}`
            pass=`grep Passphrase ${CONNMAN_CONFIG_FILE} | awk {'print $3'}`
        fi
    fi

    printf "'${ssid}' '${pass}'"
}

for opt in "$@"
do
    case "$opt" in
        -h|--help)
            print_usage
            shift ;;
        -s|--set)
            wifi_setup "$2" "$3"
            exit 0
            shift ;;
        -r|--remove)
            wifi_remove
            exit 0
            shift ;;
        -g|--get)
            show_current
            exit 0
            shift ;;
        *)
            shift ;;
    esac
done

print_usage
