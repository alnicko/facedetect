#!/usr/bin/env bash

if [[ ${OPENCV_INSTALL_PATH} == "" ]]; then
    OPENCV_INSTALL_PATH=/ust/local
fi

# get_available_governors: conservative userspace powersave ondemand performance
#sudo ./set_cpu_governor.sh performance
#sudo ./set_cpu_freq.sh 792000

#export LD_LIBRARY_PATH=${OPENCV_INSTALL_PATH}/lib

run_test()
{
    echo "With face"
    python ./facedetect_test.py ./FaceDetect     \
        --cascade=./data/haarcascade_frontalface_alt.xml    \
        ./data/lena.jpg    \
        -N=100

    echo "Without face"
    python ./facedetect_test.py ./FaceDetect     \
        --cascade=./data/haarcascade_frontalface_alt.xml    \
        ./data/lena_tmpl.jpg    \
        -N=100
}

. ./cpu_funcs.sh

echo "Start test with 2 core 792 MHz"
set_cpu_mode cpu1 on
set_cur_governor userspace
set_cpu_cur_freq cpu0 792000
get_cpu_cur_freq cpu0
run_test

echo "Start test with 2 core 996 MHz"
set_cpu_mode cpu1 on
set_cur_governor userspace
set_cpu_cur_freq cpu0 996000
get_cpu_cur_freq cpu0
run_test

echo "Start test with 1 core 792 MHz"
set_cpu_mode cpu1 off
set_cur_governor userspace
set_cpu_cur_freq cpu0 792000
get_cpu_cur_freq cpu0
run_test

echo "Start test with 1 core 996 MHz"
set_cpu_mode cpu1 off
set_cur_governor userspace
set_cpu_cur_freq cpu0 996000
get_cpu_cur_freq cpu0
run_test

set_cpu_mode cpu1 on
set_cur_governor ondemand
#sudo ./set_cpu_governor.sh ondemand
