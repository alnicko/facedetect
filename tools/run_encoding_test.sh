#!/usr/bin/env bash

# record stream
MODE=${1}

# get_available_governors: conservative userspace powersave ondemand performance
#sudo ./set_cpu_governor.sh performance
sudo ./set_cpu_freq.sh 792000

source ./cpu_funcs.sh
get_cpu_cur_freq cpu0

echo "Start test with performance governor"


if [[ "${MODE}" == "record" ]]; then
    python3.5 ./codec_test.py record
elif [[ "${MODE}" == "stream" ]]; then
    python3.5 ./codec_test.py stream rtmp://stream.solar-eco.org/hls/test_stream
fi

sudo ./set_cpu_governor.sh ondemand
