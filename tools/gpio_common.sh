#!/bin/bash

GPIO_CLASS_PATH="/sys/class/gpio"
GPIO_EXPORT="${GPIO_CLASS_PATH}/export"
GPIO_UNEXPORT="${GPIO_CLASS_PATH}/unexport"

gpio_export()
{
    gpio=${1}

    echo ${gpio} > ${GPIO_EXPORT}
}

gpio_unexport()
{
    gpio=${1}

    echo ${gpio} > ${GPIO_UNEXPORT}
}

gpio_set_direction()
{
    gpio=${1}
    direction=${2}

    echo ${direction} > ${GPIO_CLASS_PATH}/gpio${gpio}/direction
}

gpio_set_value()
{
    gpio=${1}
    val=${2}

    echo ${val} > ${GPIO_CLASS_PATH}/gpio${gpio}/value
}

gpio_setup()
{
    gpio_num=${1}
    mode=${2}
    val=${3}

    if [ ! -d ${GPIO_CLASS_PATH}/gpio${gpio_num} ]; then
        gpio_export ${gpio_num}
    fi

    gpio_set_direction ${gpio_num} ${mode}

    if [ "${mode}" == "out" ]; then
        gpio_set_value ${gpio_num} ${val}
    fi
}
