from __future__ import print_function
import os
import re
import subprocess
import sys
import time
from collections import Counter


def cpu_percent():
    try:
        import psutil as psutil
        return psutil.cpu_percent()
    except ImportError:
        return 10


def run_cmd_and_log_cpuload(cmd, env):
    cpu_usage_avg = 0
    cpu_usage_min = 100
    cpu_usage_max = 0

    cpu_percent()
    proc = subprocess.Popen(cmd, env=env, shell=True, universal_newlines=True,
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    count = 0
    while proc.poll() is None:
        # try:
        #     proc.wait(1)
        # except subprocess.TimeoutExpired:
        #     pass
        time.sleep(1)
        print(".", end='')
        cpu_usage = cpu_percent()
        cpu_usage_avg += cpu_usage
        cpu_usage_min = min(cpu_usage_min, cpu_usage)
        cpu_usage_max = max(cpu_usage_max, cpu_usage)
        count += 1
    print(".")
    if count > 0:
        cpu_usage_avg /= count

    ret = dict()
    ret['out'] = proc.stdout.readlines()
    ret['returncode'] = proc.returncode
    if proc.returncode == 0:
        ret.update({
            'cpu_usage_avg': cpu_usage_avg,
            'cpu_usage_min': cpu_usage_min,
            'cpu_usage_max': cpu_usage_max
        })
    return ret


def parse_facedetect_output(output):
    RES_REGEX = re.compile('^RES: \[(\d+ x \d+ from \(\d+, \d+\))\] dt: (\d+.?\d*)*$')
    counter = Counter()
    for line in output:
        if 'RES:' in line:
            m = RES_REGEX.match(line)
            if m is not None:
                counter[m.group(1)] += 1
                counter[m.group(2)] += 1

    faces = {}
    dt_min = 100000000
    dt_max = 0
    dt_avr = 0
    dt_count = 0
    for key in counter.keys():
        if 'from' in key:
            faces[key] = counter[key]
        else:
            dt = float(key)
            dt_avr += dt * counter[key]
            dt_count += counter[key]

            # print("dt: ", dt)
            # print("dt_c: ", counter[key])
            # print("dt_avr: ", dt_avr)
            # print("dt_count: ", dt_count)

            dt_min = min(dt_min, dt)
            dt_max = max(dt_max, dt)

    if dt_count > 0:
        dt_avr /= dt_count

    # print("=======")
    # print("dt_avr: ", dt_avr)
    # print("dt_min: ", dt_min)
    # print("dt_max: ", dt_max)
    # print("dt_count: ", dt_count)

    return {
        'faces': faces,
        'dt_avr': dt_avr,
        'dt_min': dt_min,
        'dt_max': dt_max,
        'dt_count': dt_count
    }


def parse_faces(faces):
    detected = 0
    not_detected = 0
    for face in faces.keys():
        if '0 x 0' in face:
            not_detected += faces[face]
        else:
            detected += faces[face]

    unic_detections = len(faces)
    if not_detected > 0:
        unic_detections -= 1

    return {
        'detected': detected,
        'unic_pos_count': unic_detections,
        'not_detected': not_detected
    }


if __name__ == '__main__':
    sys.argv.pop(0)
    args = ' '.join(sys.argv)

    cmd = './Debug_x86_64/FaceDetect --cascade=./data/haarcascade_frontalface_alt.xml ../data/lena.jpg -N=100'
    env = os.environ
    if env.get('LD_LIBRARY_PATH') is None:
        env['LD_LIBRARY_PATH'] = '/home/michael/OpenCV/opencv_builder/opencv-4.0.1/build_x86_64_Debug/installation/lib'

    if args == '':
        args = cmd
    out = run_cmd_and_log_cpuload(args, env)

    if out['returncode'] == 0:
        pr_out = ''
        for key in out.keys():
            if key == 'returncode':
                continue
            if key == 'out':
                facedetect_res = parse_facedetect_output(out['out'])
                print(facedetect_res)
                print(parse_faces(facedetect_res['faces']))
            else:
                pr_out = '{} {}: {}\n'.format(pr_out, key, out[key])
        print(pr_out)
    else:
        print(out)

    exit(out['returncode'])
