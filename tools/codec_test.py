import json
import os
import subprocess
import sys

import psutil as psutil

FPS_LIST = [30]
BITRATE_KB_LIST = [1000, 5000, 10000]
RESOLUTION_LIST = {
    '640x480': 0,
    '1280x720': 4,
    '1920x1080': 5
}
"""
 imx-capture-mode    : Capture mode of camera, varies with each v4l2 driver,
                                for example ov5460:
                                ov5640_mode_VGA_640_480 = 0,
                                ov5640_mode_QVGA_320_240 = 1,
                                ov5640_mode_NTSC_720_480 = 2,
                                ov5640_mode_PAL_720_576 = 3,
                                ov5640_mode_720P_1280_720 = 4,
                                ov5640_mode_1080P_1920_1080 = 5
"""

report = {
    'reports': []
}

mode = sys.argv[1]

iface = 'eth0'
record_time = 30
for resolution in RESOLUTION_LIST.keys():
    print("Testing ", resolution)

    for fps in FPS_LIST:
        print("Testing {} fps".format(fps))

        for bitrate in BITRATE_KB_LIST:
            print("Testing {}k bitrate".format(bitrate))

            cpu_usage_min = 100
            cpu_usage_max = 0
            cpu_usage_avg = 0

            if mode == 'record':
                filename = 'video_{}_{}fps_{}kb_{}sec.avi'.format(resolution, fps, bitrate, record_time)
                cmd = 'gst-launch-1.0 imxv4l2videosrc device=/dev/video0 imx-capture-mode={} fps-n={} num-buffers={} ! queue ! imxvpuenc_h264 bitrate={} ! avimux ! filesink location={}'\
                    .format(RESOLUTION_LIST[resolution], fps, record_time * fps, bitrate, filename)
            elif mode == 'stream':
                cmd = 'gst-launch-1.0 imxv4l2videosrc device=/dev/video0 imx-capture-mode={} fps-n={} num-buffers={} ! queue ! imxvpuenc_h264 bitrate={} ! h264parse ! flvmux streamable=true ! queue ! rtmpsink location={}' \
                    .format(RESOLUTION_LIST[resolution], fps, record_time * fps, bitrate, sys.argv[2])
                net_io_counters_at_start = psutil.net_io_counters(pernic=True)[iface]
            else:
                cmd = ''

            psutil.cpu_percent()
            recording_process = subprocess.Popen(cmd, preexec_fn=os.setsid, shell=True)
            count = 0
            while recording_process.poll() is None:
                try:
                    recording_process.wait(1)
                except subprocess.TimeoutExpired:
                    pass

                cpu_usage = psutil.cpu_percent()
                if cpu_usage > 0:
                    cpu_usage_avg += cpu_usage
                    cpu_usage_min = min(cpu_usage_min, cpu_usage)
                    cpu_usage_max = max(cpu_usage_max, cpu_usage)
                    count += 1

            if mode == 'stream':
                net_io_counters = psutil.net_io_counters(pernic=True)[iface]

            test_result = dict()
            test_result['resolution'] = resolution
            test_result['fps'] = fps
            test_result['bitrate'] = bitrate
            if mode == 'record':
                test_result['record_time'] = record_time
                test_result['filename'] = filename
                test_result['filename_size'] = os.path.getsize(filename)
            if mode == 'stream':
                test_result['iface_name'] = iface
                test_result['stream_time'] = record_time
                test_result['net_io_stat'] = {}
                upload = net_io_counters.bytes_sent - net_io_counters_at_start.bytes_sent
                download = net_io_counters.bytes_recv - net_io_counters_at_start.bytes_recv
                test_result['net_io_stat']['upload'] = upload
                test_result['net_io_stat']['download'] = download
                test_result['net_io_stat']['tx_rate'] = 8 * upload / record_time
                test_result['net_io_stat']['rx_rate'] = 8 * download / record_time

            test_result['cpu_usage'] = {}
            test_result['cpu_usage']['min'] = cpu_usage_min
            test_result['cpu_usage']['max'] = cpu_usage_max
            test_result['cpu_usage']['avg'] = cpu_usage_avg / count
            test_result['cpu_usage']['count'] = count

            report['reports'].append(test_result)
            print("Report: ", test_result)


with open('{}_test_report.json'.format(mode), 'w') as json_file:
    json.dump(report, json_file)
