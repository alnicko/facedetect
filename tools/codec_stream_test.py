import json
import os

import psutil as psutil


# FPS_LIST = [30]
# BITRATE_LIST = [10000000]
# RESOLUTION_LIST = ['1920x1080']
FPS_LIST = [5, 15, 30]
BITRATE_LIST = [500000, 1000000, 2000000, 5000000, 10000000]
RESOLUTION_LIST = ['640x480', '1280x720', '1920x1080']

DEVICE_ID = 'f7d64b095c634c3a923fcc5ce0379b61'
if is_artik_dev:
    vs = VideoService(dev_id=DEVICE_ID)
else:
    vs = VideoService(dev_id=DEVICE_ID, video_streamer_env=dict(os.environ, TARGET="pc"))

# vs.logger.setLevel('DEBUG')
# import coloredlogs
# coloredlogs.install(level='DEBUG')

cfg = json.loads(DEFAULT_CFG)

vs.start()

report = {
    'reports': []
}

# test_result = {
#     'resolution': '640x480',
#     'fps': 5,
#     'bitrate': 500000,
#     'stream_time': 20,
#     'cpu_usage': {
#         'min': 10,
#         'max': 20,
#         'avg': 15
#     },
#     'net_io_stat': {
#         'upload': 100,
#         'download': 100,
#         'tx_rate': 10,
#         'rx_rate': 10
#     }
# }

iface = 'wwan0'
stream_time = 20
for resolution in RESOLUTION_LIST:
    print("Testing ", resolution)

    cfg['video_cfg']['resolution'] = resolution

    for fps in FPS_LIST:
        print("Testing {} fps".format(fps))

        cfg['video_cfg']['framerate'] = fps

        for bitrate in BITRATE_LIST:
            print("Testing {} bitrate".format(bitrate))

            cfg['video_cfg']['bitrate'] = bitrate

            net_io_counters_at_start = psutil.net_io_counters(pernic=True)[iface]
            print('net_io_counters_at_start: ', net_io_counters_at_start)

            cpu_usage_min = 0
            cpu_usage_max = 0
            cpu_usage_avg = 0

            vs.set_cfg(cfg)
            vs.send_cmd(CMD_START)

            net_io_counters = psutil.net_io_counters()
            for i in range(stream_time):
                cpu_usage = psutil.cpu_percent(interval=1)
                cpu_usage_avg += cpu_usage
                cpu_usage_min = min(cpu_usage_min, cpu_usage)
                cpu_usage_max = max(cpu_usage_min, cpu_usage)

                # net_if_stats = psutil.net_if_stats()
                # sensors_temperatures = psutil.sensors_temperatures()
                net_io_counters = psutil.net_io_counters(pernic=True)[iface]

                print(cpu_usage)
                # print(net_if_stats)
                # print(sensors_temperatures)
                print(net_io_counters)

            vs.send_cmd(CMD_STOP)

            upload = net_io_counters.bytes_sent - net_io_counters_at_start.bytes_sent
            download = net_io_counters.bytes_recv - net_io_counters_at_start.bytes_recv

            test_result = dict()
            test_result['resolution'] = resolution
            test_result['fps'] = fps
            test_result['bitrate'] = bitrate
            test_result['stream_time'] = stream_time
            test_result['iface_name'] = iface
            test_result['cpu_usage'] = {}
            test_result['cpu_usage']['min'] = cpu_usage_min
            test_result['cpu_usage']['max'] = cpu_usage_max
            test_result['cpu_usage']['avg'] = cpu_usage_avg / stream_time
            test_result['net_io_stat'] = {}
            test_result['net_io_stat']['upload'] = upload
            test_result['net_io_stat']['download'] = download
            test_result['net_io_stat']['tx_rate'] = 8 * upload / stream_time
            test_result['net_io_stat']['rx_rate'] = 8 * download / stream_time

            report['reports'].append(test_result)

vs.stop()

with open('stream_test_report.json', 'w') as json_file:
    json.dump(report, json_file)
