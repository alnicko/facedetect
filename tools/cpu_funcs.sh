#!/usr/bin/env bash

CPU_PATH=/sys/devices/system/cpu
CPU_AVAILABLE_GOVERNORS=$CPU_PATH/cpu0/cpufreq/scaling_available_governors
CPU_AVAILABLE_FREQ=$CPU_PATH/cpu0/cpufreq/scaling_available_frequencies
CPU_GOVERNOR=$CPU_PATH/cpu0/cpufreq/scaling_governor
CPU_FREQ=cpufreq/scaling_cur_freq
CPU_SET_FREQ=cpufreq/scaling_setspeed
CPU_ONLINE=$CPU_PATH/online
CPU_OFFLINE=$CPU_PATH/offline


get_available_frequencies()
{
    AVAILABLE_FREQ="`cat $CPU_AVAILABLE_FREQ`"
    echo "$AVAILABLE_FREQ"
}

get_available_governors()
{
    AVAILABLE_GOVERNORS="`cat $CPU_AVAILABLE_GOVERNORS`"
    echo "$AVAILABLE_GOVERNORS"
}

get_cur_governor()
{
    GOVERNOR="`cat $CPU_GOVERNOR`"
    echo $GOVERNOR
}

set_cur_governor()
{
    GOVERNOR=$1
    echo $GOVERNOR > $CPU_GOVERNOR
}

get_cpu_cur_freq()
{
    CPU=$1

    if [[ "$CPU" == "cpu0" ]] || [[ "$CPU" == "cpu1" ]] || [[ "$CPU" == "cpu2" ]] || [[ "$CPU" == "cpu3" ]]; then
        freq="`cat $CPU_PATH/$CPU/$CPU_FREQ`"
        echo "$freq"
    else
        return 1
    fi
}

set_cpu_cur_freq()
{
    CPU=$1
    FREQ=$2

    if [[ "$CPU" == "cpu0" ]] || [[ "$CPU" == "cpu1" ]] || [[ "$CPU" == "cpu2" ]] || [[ "$CPU" == "cpu3" ]]; then
        echo $FREQ > $CPU_PATH/$CPU/$CPU_SET_FREQ
    else
        return 1
    fi
}

set_cpu_mode()
{
    CPU=$1
    MODE=$2
    # cpu0 can not be disabled! If try disable kernel will crash!
    if [[ "$CPU" == "cpu1" ]] || [[ "$CPU" == "cpu2" ]] || [[ "$CPU" == "cpu3" ]]; then
        if [[ "$MODE" == "on" ]]; then
            echo 1 > $CPU_PATH/$CPU/online
        elif [[ "$MODE" == "off" ]]; then
            echo 0 > $CPU_PATH/$CPU/online            
        else
            return 1
        fi
    else
        return 1
    fi
}

get_online_cpu()
{
    CPU="`cat $CPU_ONLINE`"
    echo $CPU
}

get_offline_cpu()
{
    CPU="`cat $CPU_OFFLINE`"
    echo $CPU
}

test_cpu_on_off()
{
    while true
    do
        cpu="cpu$((RANDOM%4))"
        mode="$((RANDOM%2))"

        if [[ $mode -eq 1 ]]; then
            set_cpu_mode $cpu on
        else
            set_cpu_mode $cpu off
        fi

        sleep 0.5
    done
}

load_cpu()
{
    while true; do
        true
    done
}
