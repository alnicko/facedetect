#!/usr/bin/env bash

PWM_CLASS_PATH="/sys/class/pwm"
PWM_CHIP_="${PWM_CLASS_PATH}/pwmchip"

pwm_release()
{
    local channel=${1}
    pwm_disable ${channel}
    echo 0 > ${PWM_CHIP_}${channel}/unexport
}

pwm_set_period()
{
    local channel=${1}
    local period=${2}
    echo ${period} > ${PWM_CHIP_}${1}/pwm0/period
}

pwm_get_period()
{
    local channel=${1}
    local period=${2}
    cat ${PWM_CHIP_}${1}/pwm0/period
}

pwm_set_duty()
{
    local channel=${1}
    local duty=${2}
    echo ${duty} > ${PWM_CHIP_}${1}/pwm0/duty_cycle
}

pwm_get_duty()
{
    local channel=${1}
    local duty=${2}
    cat ${PWM_CHIP_}${1}/pwm0/duty_cycle
}

pwm_enable()
{
    local channel=${1}
    echo 1 > ${PWM_CHIP_}${1}/pwm0/enable
}

pwm_disable()
{
    local channel=${1}
    echo 0 > ${PWM_CHIP_}${1}/pwm0/enable
}

pwm_setup()
{
    local channel=${1}
    local period=${2}
    local duty=${3}

    echo 0 > ${PWM_CHIP_}${channel}/export
    pwm_set_period ${channel} ${period}
    pwm_set_duty ${channel} ${duty}
    pwm_enable ${channel}
}