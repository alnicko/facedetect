#!/usr/bin/env bash

W=${1}
H=${2}
BITRATE=${3}
RECORD_NAME=${4}
DUTARION=${5}

if [[ ${W} == "" ]] || [[ ${H} == "" ]] || [[ ${BITRATE} == "" ]] || [[ ${RECORD_NAME} == "" ]] || [[ ${DUTARION} == "" ]]; then
    W=1920
    H=1080
    BITRATE=10000
    RECORD_NAME=vid_1920x1080_15fps_10Mb
    DUTARION=10
fi

FPS=30
if [[ ${W} -ge 1920 ]]; then
    FPS=15
fi

gst-launch-1.0 imxv4l2src device=/dev/video0 num-buffers=$((${DUTARION} * ${FPS})) \
    ! video/x-raw,format=I420,width=${W},height=${H},framerate=${FPS}/1 \
    ! queue ! vpuenc_h264 bitrate=${BITRATE} ! avimux ! filesink location=${RECORD_NAME}.avi
