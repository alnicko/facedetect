#!/usr/bin/env bash

source ./cpu_funcs.sh

set_cur_governor userspace
set_cpu_cur_freq ${1}
